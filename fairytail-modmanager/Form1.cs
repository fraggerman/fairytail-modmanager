﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;

namespace fairytail_modmanager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void RefreshModlist()
        {
            chkLst_mods.Items.Clear();
            string path = txtbx_path.Text;
            if(path.Length > 0)
            {
                path += "\\Mods";
                if(Directory.Exists(path))
                {
                    string[] dirs = Directory.GetDirectories(path);
                    foreach (string dir in dirs)
                    {
                        string[] dir_split = dir.Split("\\");
                        string modname = dir_split[^1];
                        if (modname.ToLower().StartsWith("disabled"))
                        {
                            modname = modname[8..];
                            while (modname.StartsWith(" "))
                                modname = modname[1..];
                            chkLst_mods.Items.Add(modname);
                        }
                        else
                        {
                            int index = chkLst_mods.Items.Add(modname);
                            chkLst_mods.SetItemChecked(index, true);

                        }
                    }
                }
            }
        }

        private void Btn_changePath_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog_FTFolder.ShowDialog();
            if(result == DialogResult.OK)
            {
                txtbx_path.Text = folderBrowserDialog_FTFolder.SelectedPath;
                RefreshModlist();
            }
        }

        private void Btn_refreshModlist_Click(object sender, EventArgs e)
        {
            RefreshModlist();
        }

        private void Btn_uninstall_Click(object sender, EventArgs e)
        {
            if((MessageBox.Show("Are you sure, you want to delete the selected mod?", "Delete Selected Mod", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.No))
            {
                return;
            }
            object selectedItem = chkLst_mods.SelectedItem;
            if(selectedItem != null)
            {
                int index = chkLst_mods.FindString(selectedItem.ToString());
                CheckState state = chkLst_mods.GetItemCheckState(index);
                string path = txtbx_path.Text + "\\Mods";
                if (state == CheckState.Checked)
                {
                    Directory.Delete(path + "\\" + selectedItem.ToString(), true);
                }
                else
                {
                    string pattern = @"disabled(\s?)" + selectedItem.ToString();
                    string[] dirs = Directory.GetDirectories(path);
                    foreach (string dir in dirs)
                    {
                        MatchCollection matches = Regex.Matches(dir, pattern, RegexOptions.IgnoreCase);
                        if (matches.Count > 0)
                        {
                            Directory.Delete(dir, true);
                            break;
                        }
                    }
                }
                RefreshModlist();
            }
        }

        private void Btn_uninstallAll_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Are you sure, you want to delete all installed mods?", "Delete All Mods", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.No))
            {
                return;
            }
            string path = txtbx_path.Text + "\\Mods";
            string[] dirs = Directory.GetDirectories(path);
            foreach (string dir in dirs)
            {
                Directory.Delete(dir, true);
            }
            RefreshModlist();
        }

        private void Btn_installNew_Click(object sender, EventArgs e)
        {
            string path = txtbx_path.Text + "\\Mods";
            DialogResult result = openFileDialog_newMod.ShowDialog();
            if (result == DialogResult.OK)
            {
                ZipFile.ExtractToDirectory(openFileDialog_newMod.FileName, path + "\\" + openFileDialog_newMod.SafeFileName.Substring(0, openFileDialog_newMod.SafeFileName.Length - 4));
                RefreshModlist();
            }
        }

        private void ChkLst_mods_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            string path = txtbx_path.Text + "\\Mods";
            int index = e.Index;
            CheckState newState = e.NewValue;
            string name = chkLst_mods.Items[index].ToString();
            if (newState == CheckState.Unchecked)
            {
                Directory.Move(path + "\\" + name, path + "\\DISABLED " + name);
            }
            else
            {
                string pattern = @"disabled(\s?)" + name;
                string[] dirs = Directory.GetDirectories(path);
                foreach (string dir in dirs)
                {
                    MatchCollection matches = Regex.Matches(dir, pattern, RegexOptions.IgnoreCase);
                    if (matches.Count > 0)
                    {
                        Directory.Move(dir, dir.Substring(0, dir.Length - matches[0].Length) + name);
                        break;
                    }
                }
            }
        }
    }
}
