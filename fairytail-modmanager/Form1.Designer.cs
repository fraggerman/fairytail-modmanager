﻿
namespace fairytail_modmanager
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_installNew = new System.Windows.Forms.Button();
            this.btn_uninstall = new System.Windows.Forms.Button();
            this.btn_uninstallAll = new System.Windows.Forms.Button();
            this.btn_changePath = new System.Windows.Forms.Button();
            this.lbl_list = new System.Windows.Forms.Label();
            this.chkLst_mods = new System.Windows.Forms.CheckedListBox();
            this.folderBrowserDialog_FTFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.txtbx_path = new System.Windows.Forms.TextBox();
            this.lbl_path = new System.Windows.Forms.Label();
            this.openFileDialog_newMod = new System.Windows.Forms.OpenFileDialog();
            this.btn_refreshModlist = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_installNew
            // 
            this.btn_installNew.Location = new System.Drawing.Point(496, 27);
            this.btn_installNew.Name = "btn_installNew";
            this.btn_installNew.Size = new System.Drawing.Size(312, 41);
            this.btn_installNew.TabIndex = 1;
            this.btn_installNew.Text = "Install New Mod";
            this.btn_installNew.UseVisualStyleBackColor = true;
            this.btn_installNew.Click += new System.EventHandler(this.Btn_installNew_Click);
            // 
            // btn_uninstall
            // 
            this.btn_uninstall.Location = new System.Drawing.Point(496, 74);
            this.btn_uninstall.Name = "btn_uninstall";
            this.btn_uninstall.Size = new System.Drawing.Size(312, 41);
            this.btn_uninstall.TabIndex = 2;
            this.btn_uninstall.Text = "Uninstall Selected Mod";
            this.btn_uninstall.UseVisualStyleBackColor = true;
            this.btn_uninstall.Click += new System.EventHandler(this.Btn_uninstall_Click);
            // 
            // btn_uninstallAll
            // 
            this.btn_uninstallAll.Location = new System.Drawing.Point(496, 121);
            this.btn_uninstallAll.Name = "btn_uninstallAll";
            this.btn_uninstallAll.Size = new System.Drawing.Size(312, 41);
            this.btn_uninstallAll.TabIndex = 3;
            this.btn_uninstallAll.Text = "Uninstall All Mods";
            this.btn_uninstallAll.UseVisualStyleBackColor = true;
            this.btn_uninstallAll.Click += new System.EventHandler(this.Btn_uninstallAll_Click);
            // 
            // btn_changePath
            // 
            this.btn_changePath.Location = new System.Drawing.Point(496, 332);
            this.btn_changePath.Name = "btn_changePath";
            this.btn_changePath.Size = new System.Drawing.Size(312, 41);
            this.btn_changePath.TabIndex = 5;
            this.btn_changePath.Text = "Change FairyTail Installation Path";
            this.btn_changePath.UseVisualStyleBackColor = true;
            this.btn_changePath.Click += new System.EventHandler(this.Btn_changePath_Click);
            // 
            // lbl_list
            // 
            this.lbl_list.AutoSize = true;
            this.lbl_list.Location = new System.Drawing.Point(12, 9);
            this.lbl_list.Name = "lbl_list";
            this.lbl_list.Size = new System.Drawing.Size(84, 15);
            this.lbl_list.TabIndex = 6;
            this.lbl_list.Text = "Installed Mods";
            // 
            // chkLst_mods
            // 
            this.chkLst_mods.FormattingEnabled = true;
            this.chkLst_mods.Location = new System.Drawing.Point(12, 27);
            this.chkLst_mods.Name = "chkLst_mods";
            this.chkLst_mods.Size = new System.Drawing.Size(478, 346);
            this.chkLst_mods.TabIndex = 7;
            this.chkLst_mods.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ChkLst_mods_ItemCheck);
            // 
            // folderBrowserDialog_FTFolder
            // 
            this.folderBrowserDialog_FTFolder.RootFolder = System.Environment.SpecialFolder.ProgramFiles;
            this.folderBrowserDialog_FTFolder.SelectedPath = "C:\\Program Files\\Steam\\steamapps\\common\\FAIRY TAIL";
            // 
            // txtbx_path
            // 
            this.txtbx_path.Location = new System.Drawing.Point(496, 303);
            this.txtbx_path.Name = "txtbx_path";
            this.txtbx_path.ReadOnly = true;
            this.txtbx_path.Size = new System.Drawing.Size(312, 23);
            this.txtbx_path.TabIndex = 8;
            // 
            // lbl_path
            // 
            this.lbl_path.AutoSize = true;
            this.lbl_path.Location = new System.Drawing.Point(496, 285);
            this.lbl_path.Name = "lbl_path";
            this.lbl_path.Size = new System.Drawing.Size(137, 15);
            this.lbl_path.TabIndex = 9;
            this.lbl_path.Text = "FairyTail Installation Path";
            // 
            // openFileDialog_newMod
            // 
            this.openFileDialog_newMod.DefaultExt = "zip";
            this.openFileDialog_newMod.FileName = "openFileDialog1";
            // 
            // btn_refreshModlist
            // 
            this.btn_refreshModlist.Location = new System.Drawing.Point(496, 168);
            this.btn_refreshModlist.Name = "btn_refreshModlist";
            this.btn_refreshModlist.Size = new System.Drawing.Size(312, 41);
            this.btn_refreshModlist.TabIndex = 10;
            this.btn_refreshModlist.Text = "Refresh Mod List";
            this.btn_refreshModlist.UseVisualStyleBackColor = true;
            this.btn_refreshModlist.Click += new System.EventHandler(this.Btn_refreshModlist_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 381);
            this.Controls.Add(this.btn_refreshModlist);
            this.Controls.Add(this.lbl_path);
            this.Controls.Add(this.txtbx_path);
            this.Controls.Add(this.chkLst_mods);
            this.Controls.Add(this.lbl_list);
            this.Controls.Add(this.btn_changePath);
            this.Controls.Add(this.btn_uninstallAll);
            this.Controls.Add(this.btn_uninstall);
            this.Controls.Add(this.btn_installNew);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "FairyTail Modmanager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_installNew;
        private System.Windows.Forms.Button btn_uninstall;
        private System.Windows.Forms.Button btn_uninstallAll;
        private System.Windows.Forms.Button btn_changePath;
        private System.Windows.Forms.Label lbl_list;
        private System.Windows.Forms.CheckedListBox chkLst_mods;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_FTFolder;
        private System.Windows.Forms.TextBox txtbx_path;
        private System.Windows.Forms.Label lbl_path;
        private System.Windows.Forms.OpenFileDialog openFileDialog_newMod;
        private System.Windows.Forms.Button btn_refreshModlist;
    }
}

